function love.conf(t)
	t.title = "EdBlock"
  t.window.icon = "edblock.ico"
	t.version = "0.10.0"
	t.window.width = 1366
	t.window.height = 768
  t.identity = "EdBlock"
	t.console = false
  t.window.highdpi = false
  t.window.srgb = false
  t.window.vsync = false
  t.modules.joystick = false
  t.modules.physics = false
  t.externalstorage = false 
end