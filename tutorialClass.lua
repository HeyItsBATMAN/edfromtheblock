local tutorialClass = {}
local popupClass = require("popupClass")
local active = 1 
function tutorialClass.click(x,y)
  if x > wwidth/2 then
    if active < 4 then
      active = active + 1
    elseif active == 4 then
      tutorialcompleted = true
      love.filesystem.write(tutorialcompletedfile:getFilename(),"true")
      timers.gentimer = timer.every(gentimerinterval,function () popupClass.generate() end)
    end
  elseif x < wwidth/2 then
    if active > 1 then
      active = active - 1
    end
  end
end

function tutorialClass.draw()
  love.graphics.setColor(255,255,255,255)
  love.graphics.draw(tutorials[active],wwidth/2-(350*0.9),wheight*1/14,0,0.8)
end
return tutorialClass