local achievementClass = {}
achievements = {}

newachievement = {string="First Day At Work",icon=love.graphics.newImage("textures/icons/achievements/first_day_at_work.png"),prequirement=0,brequirement=1,crequirement=1,hrequirement=1,bombs=0,reward=1.0,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)

newachievement = {string="Earn 10 Highscore",icon=love.graphics.newImage("textures/icons/achievements/10_highscore.png"),prequirement=0,brequirement=0,crequirement=0,hrequirement=10,bombs=0,reward=1.0,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Earn 50 Highscore",icon=love.graphics.newImage("textures/icons/achievements/50_highscore.png"),prequirement=0,brequirement=0,crequirement=0,hrequirement=50,bombs=0,reward=1.0,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Earn 100 Highscore",icon=love.graphics.newImage("textures/icons/achievements/100_highscore.png"),prequirement=0,brequirement=0,crequirement=0,hrequirement=100,bombs=0,reward=1.0,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Earn 250 Highscore",icon=love.graphics.newImage("textures/icons/achievements/250_highscore.png"),prequirement=0,brequirement=0,crequirement=0,hrequirement=250,bombs=0,reward=1.0,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Earn 500 Highscore",icon=love.graphics.newImage("textures/icons/achievements/500_highscore.png"),prequirement=0,brequirement=0,crequirement=0,hrequirement=500,bombs=0,reward=1.0,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Earn 1000 Highscore",icon=love.graphics.newImage("textures/icons/achievements/1000_highscore.png"),prequirement=0,brequirement=0,crequirement=0,hrequirement=1000,bombs=0,reward=1.0,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Earn 2500 Highscore",icon=love.graphics.newImage("textures/icons/achievements/2500_highscore.png"),prequirement=0,brequirement=0,crequirement=0,hrequirement=2500,bombs=0,reward=1.0,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Earn 5000 Highscore",icon=love.graphics.newImage("textures/icons/achievements/5000_highscore.png"),prequirement=0,brequirement=0,crequirement=0,hrequirement=5000,bombs=0,reward=1.0,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Earn 10k Highscore",icon=love.graphics.newImage("textures/icons/achievements/10k_highscore.png"),prequirement=0,brequirement=0,crequirement=0,hrequirement=10000,bombs=0,reward=1.0,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Earn 25k Highscore",icon=love.graphics.newImage("textures/icons/achievements/25k_highscore.png"),prequirement=0,brequirement=0,crequirement=0,hrequirement=25000,bombs=0,reward=1.0,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Earn 50k Highscore",icon=love.graphics.newImage("textures/icons/achievements/50k_highscore.png"),prequirement=0,brequirement=0,crequirement=0,hrequirement=50000,bombs=0,reward=1.0,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Earn 100k Highscore",icon=love.graphics.newImage("textures/icons/achievements/100k_highscore.png"),prequirement=0,brequirement=0,crequirement=0,hrequirement=100000,bombs=0,reward=1.0,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Earn 250k Highscore",icon=love.graphics.newImage("textures/icons/achievements/250k_highscore.png"),prequirement=0,brequirement=0,crequirement=0,hrequirement=250000,bombs=0,reward=1.0,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Earn 500k Highscore",icon=love.graphics.newImage("textures/icons/achievements/500k_highscore.png"),prequirement=0,brequirement=0,crequirement=0,hrequirement=500000,bombs=0,reward=1.0,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Earn 1M Highscore",icon=love.graphics.newImage("textures/icons/achievements/1_million_highscore.png"),prequirement=0,brequirement=0,crequirement=0,hrequirement=1000000,bombs=0,reward=1.0,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)


newachievement = {string="Earn 100 BitCoins",prequirement=0,brequirement=100,hrequirement=0,crequirement=0,reward=1.5,rewardstring="+50% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth(),icon=love.graphics.newImage("textures/icons/achievements/100_bitcoins.png")}
table.insert(achievements,newachievement)
newachievement = {string="Earn 1000 BitCoins",prequirement=0,brequirement=1000,hrequirement=0,crequirement=0,reward=1.5,rewardstring="+50% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth(),icon=love.graphics.newImage("textures/icons/achievements/1000_bitcoins.png")}
table.insert(achievements,newachievement)
newachievement = {string="Earn 10k BitCoins",prequirement=0,brequirement=10000,hrequirement=0,crequirement=0,reward=1.5,rewardstring="+50% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth(),icon=love.graphics.newImage("textures/icons/achievements/10k_bitcoins.png")}
table.insert(achievements,newachievement)
newachievement = {string="Earn 100k BitCoins",prequirement=0,brequirement=100000,hrequirement=0,crequirement=0,reward=1.5,rewardstring="+50% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth(),icon=love.graphics.newImage("textures/icons/achievements/100k_bitcoins.png")}
table.insert(achievements,newachievement)
newachievement = {string="Earn 1m BitCoins",prequirement=0,brequirement=1000000,hrequirement=0,crequirement=0,reward=1.5,rewardstring="+50% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth(),icon=love.graphics.newImage("textures/icons/achievements/1_million_bitcoins.png")}
table.insert(achievements,newachievement)
newachievement = {string="Earn 10m BitCoins",prequirement=0,brequirement=10000000,hrequirement=0,hrequirement=0,crequirement=0,reward=1.5,rewardstring="+50% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth(),icon=love.graphics.newImage("textures/icons/achievements/10_million_bitcoins.png")}
table.insert(achievements,newachievement)
newachievement = {string="Earn 100m BitCoins",prequirement=0,brequirement=100000000,hrequirement=0,crequirement=0,reward=1.5,rewardstring="+50% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth(),icon=love.graphics.newImage("textures/icons/achievements/100_million_bitcoins.png")}
table.insert(achievements,newachievement)
newachievement = {string="Earn 1 Billion BitCoins",prequirement=0,brequirement=1000000000,crequirement=0,hrequirement=0,reward=1.5,rewardstring="+50% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth(),icon=love.graphics.newImage("textures/icons/achievements/1_billion_bitcoins.png")}
table.insert(achievements,newachievement)

newachievement = {string="10x Combo Multiplier",icon=love.graphics.newImage("textures/icons/achievements/10_combo.png"),prequirement=0,brequirement=0,crequirement=10,hrequirement=0,reward=1.1,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="50x Combo Multiplier",icon=love.graphics.newImage("textures/icons/achievements/50_combo.png"),prequirement=0,brequirement=0,crequirement=50,hrequirement=0,reward=1.5,rewardstring="+50% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="100x Combo Multiplier",icon=love.graphics.newImage("textures/icons/achievements/100_combo.png"),prequirement=0,brequirement=0,crequirement=100,hrequirement=0,reward=1.1,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="250x Combo Multiplier",icon=love.graphics.newImage("textures/icons/achievements/250_combo.png"),prequirement=0,brequirement=0,crequirement=250,hrequirement=0,reward=1.5,rewardstring="+50% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="500x Combo Multiplier",icon=love.graphics.newImage("textures/icons/achievements/500_combo.png"),prequirement=0,brequirement=0,crequirement=500,hrequirement=0,reward=1.7,rewardstring="+70% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="1000x Combo Multiplier",icon=love.graphics.newImage("textures/icons/achievements/1000_combo.png"),prequirement=0,brequirement=0,crequirement=1000,hrequirement=0,reward=1.1,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="2000x Combo Multiplier",icon=love.graphics.newImage("textures/icons/achievements/2000_combo.png"),prequirement=0,brequirement=0,crequirement=2000,hrequirement=0,reward=1.5,rewardstring="+50% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="4000x Combo Multiplier",icon=love.graphics.newImage("textures/icons/achievements/4000_combo.png"),prequirement=0,brequirement=0,crequirement=4000,hrequirement=0,reward=1.7,rewardstring="+70% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="9001x Combo Multiplier",icon=love.graphics.newImage("textures/icons/achievements/9001_combo.png"),prequirement=0,brequirement=0,crequirement=9001,hrequirement=0,reward=1.1,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)

newachievement = {string="BOOM",icon=love.graphics.newImage("textures/icons/achievements/boom.png"),prequirement=999999999,brequirement=999999999,crequirement=999999999,hrequirement=0,bombs=1,reward=1.1,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Bomb 5 PopUps",icon=love.graphics.newImage("textures/icons/achievements/blaster_5.png"),prequirement=999999999,brequirement=999999999,crequirement=999999999,hrequirement=0,bombs=5,reward=1.1,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Bomb 10 PopUps",icon=love.graphics.newImage("textures/icons/achievements/blaster_10.png"),prequirement=999999999,brequirement=999999999,crequirement=999999999,hrequirement=0,bombs=10,reward=1.1,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Bomb 15 PopUps",icon=love.graphics.newImage("textures/icons/achievements/blaster_15.png"),prequirement=999999999,brequirement=999999999,crequirement=999999999,hrequirement=0,bombs=15,reward=1.1,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Bomb 25 PopUps",icon=love.graphics.newImage("textures/icons/achievements/blaster_25.png"),prequirement=999999999,brequirement=999999999,crequirement=999999999,hrequirement=0,bombs=25,reward=1.1,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Bomb 50 PopUps",icon=love.graphics.newImage("textures/icons/achievements/blaster_50.png"),prequirement=999999999,brequirement=999999999,crequirement=999999999,hrequirement=0,bombs=50,reward=1.1,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Bomb 100 PopUps",icon=love.graphics.newImage("textures/icons/achievements/master_blaster.png"),prequirement=999999999,brequirement=999999999,crequirement=999999999,hrequirement=0,bombs=100,reward=1.1,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)

newachievement = nil

--[[newachievement = {string="Close 10 PopUps",prequirement=10,brequirement=0,crequirement=0,reward=1.1,rewardstring="+10% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Close 15 PopUps",prequirement=15,brequirement=0,crequirement=0,reward=1.2,rewardstring="+20% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)
newachievement = {string="Close 20 PopUps",prequirement=20,brequirement=0,crequirement=0,reward=1.3,rewardstring="+30% on your current Bitcoins",completed=false,transparency=0,x=love.graphics.getWidth()}
table.insert(achievements,newachievement)]]--

function achievementClass.update(dt)
  for i,achievement in ipairs(achievements) do
    if totalclosed >= achievement.prequirement and bitcoins >= achievement.brequirement and combomult >= achievement.crequirement and achievement.completed == false and highscore >= achievement.hrequirement then
      achievement.completed = true
      love.filesystem.write(completedachievements[i]:getFilename(),"true")
      bitcoins = math.ceil(bitcoins * achievement.reward)
      flux.to(achievement,0.5,{transparency=255})
      if quickmenu.opened == true then 
        flux.to(achievement,0.5,{x=wwidth-200})
      else
        flux.to(achievement,0.5,{x=wwidth-412})
      end
    end
    if achievement.completed == true and achievement.transparency==255 then
      applause[love.math.random(1,4)]:play()
      flux.to(achievement,0.5,{transparency=0}):delay(2)
      flux.to(achievement,0.5,{x=wwidth}):ease("quartin"):delay(2)
    end
  end
end
function achievementClass.draw()
  for _, achievement in ipairs(achievements) do
    love.graphics.setColor(255,255,255)
    if achievement.icon then
      love.graphics.draw(achievement.icon,achievement.x,30,0,0.39)
    end
  end
end
return achievementClass