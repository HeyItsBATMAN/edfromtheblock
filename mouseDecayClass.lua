local mouseDecayClass = {}
function mouseDecayClass.add(posX,posY,showcoins)
  newmousedecay = {mx=posX,my=posY,decay=true,transparency=255,addbitcoins=showcoins}
  table.insert(mousedecay,newmousedecay)
end
function mouseDecayClass.draw()
  for _,mouse in pairs(mousedecay) do
    if mouse.transparency > 0 then
      if mouse.addbitcoins == true then
        love.graphics.setFont(bigfont)
        if addedcoins > 0 then 
          love.graphics.setColor(255,255,255,mouse.transparency)
          love.graphics.draw(textures.bitcoin,mouse.mx+18*windowscalex,mouse.my-170*windowscaley,0,0.15*windowscalex)
          love.graphics.print("+" .. addedcoins,mouse.mx+60*windowscalex,mouse.my-176*windowscaley)
        elseif addedcoins < 0 then
          love.graphics.setColor(234,67,53,mouse.transparency)
          love.graphics.draw(textures.bitcoin,mouse.mx+18*windowscalex,mouse.my-170*windowscaley,0,0.15*windowscalex)
          love.graphics.print(addedcoins,mouse.mx+60*windowscalex,mouse.my-176*windowscaley)
          love.graphics.setFont(smallfont)
          return
        end
        love.graphics.setFont(smallfont)
      end
      if combo.combosuccess > 0 then
        love.graphics.setColor(50,255,50,mouse.transparency)
      elseif combo.combofail > 0 then
        love.graphics.setColor(255,50,50,mouse.transparency)
      end
      love.graphics.draw(combotext,mouse.mx+20*windowscalex,mouse.my-130*windowscaley)
    end
  end
end
return mouseDecayClass