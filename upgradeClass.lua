local upgradeClass = {}
local popupClass = require("popupClass")
upgradeMenu = {opened = false, x=love.graphics.getWidth(),y=0,width=100,height=love.graphics.getHeight()}
upgrades = {}
-- Quickmenu --
quickmenu = {opened = false, x=love.graphics.getWidth(),y=0,width=300,height=love.graphics.getHeight()}
quickmenuitems = {}
newquickmenu = {opened = false,name="bitcoinmining",icon=love.graphics.newImage("textures/icons/bitcoinmining.png"),size=64,x=quickmenu.x+16,y=love.graphics.getHeight()*1/9}
table.insert(quickmenuitems,newquickmenu)
newquickmenu = {opened = false,name="shop",icon=love.graphics.newImage("textures/icons/shop.png"),size=64,x=quickmenu.x+16,y=love.graphics.getHeight()*3/9}
table.insert(quickmenuitems,newquickmenu)
newquickmenu = {opened = false,name="stats",icon=love.graphics.newImage("textures/icons/stats.png"),size=64,x=quickmenu.x+16,y=love.graphics.getHeight()*5/9}
table.insert(quickmenuitems,newquickmenu)
newquickmenu = {opened = false,name="options",icon=love.graphics.newImage("textures/icons/options.png"),size=64,x=quickmenu.x+16,y=love.graphics.getHeight()*7/9}
table.insert(quickmenuitems,newquickmenu)
newquickmenu = nil
--
local closeButton = {icon=love.graphics.newImage("textures/icons/close.png"),x=quickmenu.x+10,y=576,width=64,height=64}
--
local newupgrade = {text="RAM",texture=textures.ramup,level=0,cost=10,x=upgradeMenu.x+10,y=0,width=280,height=50,stat="ram",basecost=10,multiplier=1.18147}
table.insert(upgrades,newupgrade)
--newupgrade = {text="Upgrade your Bandwidth",texture=textures.download,showplus=true,scale=0.15,level=0,cost=200000,x=upgradeMenu.x+10,y=0,width=280,height=50,stat="interval",basecost=2000000,multiplier=1.15884}
--table.insert(upgrades,newupgrade)
--newupgrade = {text="Gain more Combo",level=0,maxlevel=10,cost=50,x=upgradeMenu.x+10,y=0,width=280,height=50,stat="comboupgrade",basecost=50,multiplier=1.5}
--table.insert(upgrades,newupgrade)
newupgrade = {text= "Buy Bombs",texture=textures.bomb,scale=0.15, description = "Buy Bombs!", cost=10000,maxlevel = 5, x=upgradeMenu.x+10,y=0,width=280,height=50,basecost=10000,multiplier=2,stat="bombbuy"}
table.insert(upgrades,newupgrade) 
newupgrade = {text= "Upgrade Bomb",texture=textures.bomb,showplus=true,scale=0.15, description = "Upgrade Bombs!",level=0,maxlevel=20, cost=3000, x=upgradeMenu.x+10,y=0,width=280,height=50,basecost=8000,multiplier=1.3911,stat="bombdamage"}
table.insert(upgrades,newupgrade) 
newupgrade = {text= "Buy Life",texture=textures.kopf,showplus=true,scale=0.28, description = "Buy Life!",level=0,maxlevel=3, cost=10000, x=upgradeMenu.x+10,y=0,width=280,height=50,basecost=10000,multiplier=1,stat="life"}
table.insert(upgrades,newupgrade) 
newupgrade = {text= "Multiclicker",texture=textures.multiclicker,showplus=true,scale=0.15, description = "Multiclicker!",level=0,maxlevel=8, cost=5000, x=upgradeMenu.x+10,y=0,width=280,height=50,basecost=5000,multiplier=1.59,stat="multiclicker"}
table.insert(upgrades,newupgrade)
newupgrade = nil
-- Options --
local optionsmenu = {x=love.graphics.getWidth()}
local options = {}
newoption = {name="Background Brightness",value=2,min=1,max=4,y=0}
table.insert(options,newoption)
--newoption = {name="Background",value=2,min=1,max=5,y=0}
--table.insert(options,newoption)
newoption = {name="Volume",value=2,min=0,max=10,y=0}
table.insert(options,newoption)
newoption = nil
-- Stats --
local statsmenu = {x=love.graphics.getWidth()}
stats = {}
newstat = {text="Highest Combo",value=0,x=quickmenu.x+16,y=0}
table.insert(stats,newstat)
newstat = {text="Stage",value=0,x=quickmenu.x+16,y=0}
table.insert(stats,newstat)
--newstat = {text="Total Bitcoins Earned",value=0,x=quickmenu.x+16,y=0}
--table.insert(stats,newstat)
newstat = {text="Clicks On Popups",value=0,x=quickmenu.x+16,y=0}
table.insert(stats,newstat)
newstat = {text="Global Bitcoins",value=0,x=quickmenu.x+16,y=0}
table.insert(stats,newstat)
newstat = {text="Global Highscore",value=0,x=quickmenu.x+16,y=0}
table.insert(stats,newstat)
newstat = nil
-- Backgrounds --
local backgroundmenu = {x=love.graphics.getWidth()}
bgs = {}
newbg = {text="Meadow",unlocked=false,cost=50,number=1}
table.insert(bgs,newbg)
newbg = {text="Colorful Sands",unlocked=true,cost=50,number=2}
table.insert(bgs,newbg)
newbg = {text="Africa",unlocked=false,cost=50,number=3}
table.insert(bgs,newbg)
newbg = {text="Alpine",unlocked=false,cost=50,number=4}
table.insert(bgs,newbg)
newbg = {text="Snowflakes",unlocked=false,cost=50,number=5}
table.insert(bgs,newbg)
newbg = nil

local achievementcanvas = love.graphics.newCanvas(300,1200)
dragging = {active = false, diffY = 0,y = 0}


function upgradeClass.openMenu()
  if quickmenu.opened == false then
    quickmenu.opened = true
    flux.to(quickmenu,0.3,{x=love.graphics.getWidth()-88}):ease("expoout")
  end
end
function upgradeClass.closeMenu()
  dragging.y = 0
  --[[if quickmenu.opened == true then
    quickmenu.opened = false
    flux.to(closeButton,0.5,{x=quickmenu.x+200})
    flux.to(quickmenu,1,{x=wwidth}):ease("expoout")
  end]]--
  if quickmenuitems[1].opened == true then
    quickmenuitems[1].opened = false
    for _,_ in ipairs(bgs) do
      flux.to(backgroundmenu,0.1,{x=wwidth})
    end
    upgradeClass.openMenu()
  end
  if quickmenuitems[2].opened == true then
    quickmenuitems[2].opened = false
    for _,upgrade in ipairs(upgrades) do
      flux.to(upgrade,0.2,{x=wwidth})
    end
    upgradeClass.openMenu()
    end
  if quickmenuitems[3].opened == true then
    quickmenuitems[3].opened = false
    for _,stat in ipairs(stats) do
      flux.to(stat,0.2,{x=wwidth})
    end
    upgradeClass.openMenu()
  end
  if quickmenuitems[4].opened == true then
    quickmenuitems[4].opened = false
    for _,_ in ipairs(options) do
      flux.to(optionsmenu,0.1,{x=wwidth})
    end
    upgradeClass.openMenu()
  end
end
function upgradeClass.click(x,y)
  if x > closeButton.x and x < closeButton.x+closeButton.width and y > closeButton.y and y < closeButton.y+closeButton.height then
    upgradeClass.closeMenu()
    menu.back:stop()
    menu.back:play()
  else
    -- Backgrounds --
    for i,bg in ipairs(bgs) do
      if x > backgroundmenu.x+20 and x < backgroundmenu.x + 280 and y > (i-1)*130+dragging.y  and y < (i-1)*130+126+dragging.y then
        upgradeClass.buyBG(i)
      end
    end
    -- Upgrades --
    for i,upgrade in ipairs(upgrades) do
      if x > upgrade.x and x < upgrade.x + upgrade.width and y > upgrade.y and y < upgrade.y+upgrade.height then
        upgradeClass.buyUpgrade(i)
      end
    end
    -- Optionen --
    for i,option in ipairs(options) do
      if x > optionsmenu.x+20 and x < optionsmenu.x+20+78 and y > option.y+40 and y < option.y+140 then
        if option.value > option.min then
          option.value = option.value - 1
        end
      elseif x > optionsmenu.x+200 and x < optionsmenu.x+200+78 and y > option.y+40 and y < option.y+140 then
        if option.value < option.max then
          option.value = option.value + 1
        end
      end
      if i == 1 then
        brightness = option.value
      elseif i == 2 then
        volume = option.value/10
        
      elseif i == 3 then
        activebackground = option.value
      end
    end
    -- Quickmenu --
    for _,quickmenuit in ipairs(quickmenuitems) do
      if x > quickmenuit.x and x < quickmenuit.x + quickmenuit.size and y > quickmenuit.y and y < quickmenuit.y+quickmenuit.size and quickmenuit.opened == false and quickmenu.opened == true then
        menu.select:stop()
        menu.select:play()
        quickmenuit.opened = true
        quickmenu.opened = false
        flux.to(quickmenu,0.15,{x=wwidth}):ease("quadout"):after(quickmenu,0.6,{x=love.graphics.getWidth()-300}):ease("quadout")
      end
    end
  end
end
function upgradeClass.buyBG(index)
  if bgs[index].unlocked == false then
    if bitcoins >= bgs[index].cost then
      bitcoins = bitcoins - bgs[index].cost
      bgs[index].unlocked = true
      buysound:stop()
      buysound:play()
    end
  elseif bgs[index].unlocked == true then
    activebackground = bgs[index].number
  end
end
function upgradeClass.buyUpgrade(index)
  if bitcoins >= upgrades[index].cost and not upgrades[index].maxlevel then
    buysound:stop()
    buysound:play()
    bitcoins = bitcoins - upgrades[index].cost
    if upgrades[index].level then
      upgrades[index].level = upgrades[index].level + 1
      upgrades[index].cost = math.floor(upgrades[index].basecost * math.pow(upgrades[index].multiplier,upgrades[index].level))
    end
    if upgrades[index].stat == "ram" then
      maxpopups = maxpopups + 2
      ramup = ramup + 1
    elseif upgrades[index].stat == "interval" then
      if upgrades[index].level <= 100 then
      gentimerinterval = 60/(20+upgrades[index].level)
      end
      timer.cancel(timers.gentimer)
      timers.gentimer = timer.every(gentimerinterval,function () popupClass.generate() end)
      speedup = speedup + 1
    end
  elseif bitcoins >= upgrades[index].cost and upgrades[index].maxlevel and upgrades[index].level < upgrades[index].maxlevel then
    if upgrades[index].stat == "bombbuy" and canbuybomb == false then
      return
    elseif upgrades[index].stat == "bombbuy" and canbuybomb == true then
      timer.during(120, function() canbuybomb = false
end, function()
      canbuybomb = true end)
      canbuybombtimer = 120
      timer.every(1, function() canbuybombtimer = canbuybombtimer - 1 end,120 )
    end
    buysound:stop()
    buysound:play()
    bitcoins = bitcoins - upgrades[index].cost
    upgrades[index].level = upgrades[index].level + 1
    upgrades[index].cost = math.floor(upgrades[index].basecost * math.pow(upgrades[index].multiplier,upgrades[index].level))
    if upgrades[index].stat == "bombbuy" and bomb < upgrades[index].maxlevel then
      bomb = bomb + 1
      upgrades[index].level = bomb
    elseif upgrades[index].stat == "bombbuy" and bomb >= upgrades[index].maxlevel then
      bitcoins = bitcoins + upgrades[index].cost
    end
    if upgrades[index].stat == "life" and lives < upgrades[index].maxlevel then
      lives = lives + 1
      upgrades[index].level = lives
    elseif upgrades[index].stat == "life" and lives >= upgrades[index].maxlevel then
      bitcoins = bitcoins + upgrades[index].cost
    end
    if upgrades[index].stat == "comboupgrade" then
      comboupgrade = comboupgrade + 0.1
    end
    if upgrades[index].stat == "bombdamage" then
      bombdamage = bombdamage + 1
    end
  end
end
function upgradeClass.update()
  for index,upgrade in ipairs(upgrades) do
    upgrades[index].cost = math.floor(upgrades[index].basecost * math.pow(upgrades[index].multiplier,upgrades[index].level))
  end
  if quickmenu.opened == true then
    flux.to(closeButton,0.01,{y=wheight+10})
    closeButton.x=quickmenu.x+10
    
    for _,quickmenuit in ipairs(quickmenuitems) do
      quickmenuit.x = quickmenu.x+10
    end
  end
  if quickmenuitems[1].opened == true then
    flux.to(backgroundmenu,0.1,{x=quickmenu.x})
    flux.to(closeButton,0.01,{y=dragging.y+680})
    flux.to(closeButton,0.01,{x=quickmenu.x+200})
    
    
  end
  if quickmenuitems[2].opened == true then
    flux.to(closeButton,0.01,{y=wheight-80})
    flux.to(closeButton,0.01,{x=quickmenu.x+200})
    
    for i,upgrade in ipairs(upgrades) do
    upgrade.x = quickmenu.x+10
      if upgrade.y == 0 and i == 1 then
        upgrade.y = 10
      elseif upgrade.y == 0 and i > 1 then
        upgrade.y = upgrades[i-1].y + upgrades[i-1].height + 10
      end
    end
  elseif quickmenuitems[2].opened == false then
    for _,upgrade in ipairs(upgrades) do
      flux.to(upgrade,0.2,{x=wwidth})
    end
  end
  if quickmenuitems[3].opened == true then
    flux.to(closeButton,0.01,{y=dragging.y+880})
    flux.to(closeButton,0.01,{x=quickmenu.x+200})
    for _,_ in ipairs(options) do
      flux.to(statsmenu,0.1,{x=quickmenu.x})
    end
    for i,stat in ipairs(stats) do
    stat.x = quickmenu.x+10
      if stat.y == 0 and i == 1 then
        stat.y = 20
      elseif stat.y == 0 and i > 1 then
        stat.y = stats[i-1].y + 30
      end
    end
  end
  if quickmenuitems[4].opened == true then
    flux.to(optionsmenu,0.1,{x=quickmenu.x})
    for i,option in ipairs(options) do
      
      if option.y == 0 then
        option.y = (i-1)*200
      end
    end
    flux.to(closeButton,0.01,{y=wheight-80})
    flux.to(closeButton,0.01,{x=quickmenu.x+200})
  end
end
function upgradeClass.updateOptions(values)
  options[1].value = values[1]
  options[2].value = values[2]

end
function upgradeClass.updateStats(values)
  stats[1].value = values[4]
  stats[2].value = values[5]
  --stats[3].value = values[2]
  stats[3].value = values[1]
  stats[4].value = values[6]
  stats[5].value = values[7]
  upgrades[4].level = values[8]
end
function upgradeClass.draw()
  
  love.graphics.setColor(0,0,0,200)
  love.graphics.rectangle("fill",quickmenu.x,quickmenu.y,quickmenu.width,quickmenu.height)
  love.graphics.setColor(255,255,255)
  love.graphics.draw(closeButton.icon,closeButton.x,closeButton.y,0,0.25)
  for i, bg in ipairs(bgs) do
    love.graphics.setColor(255,255,255,100)
    love.graphics.rectangle("fill",backgroundmenu.x+20,(i-1)*130+dragging.y,260,122,12,12)
    love.graphics.setColor(255,255,255)
    love.graphics.print(bg.text,backgroundmenu.x+10+24,(i-1)*130+dragging.y)
    if bg.number == 1 then
      love.graphics.draw(backgrounds.wiese,backgroundmenu.x+10+24,24+2+(i-1)*130+dragging.y,0,0.12)
    elseif bg.number == 2 then
      love.graphics.draw(backgrounds.colorful_sands,backgroundmenu.x+10+24,24+2+(i-1)*130+dragging.y,0,0.12)
    elseif bg.number == 3 then
      love.graphics.draw(backgrounds.africa,backgroundmenu.x+10+24,24+2+(i-1)*130+dragging.y,0,0.12)
    elseif bg.number == 4 then
      love.graphics.draw(backgrounds.alpine,backgroundmenu.x+10+24,24+2+(i-1)*130+dragging.y,0,0.12)
    elseif bg.number == 5 then
      love.graphics.draw(backgrounds.snowflakes,backgroundmenu.x+10+24,24+2+(i-1)*130+dragging.y,0,0.12)
    end
    
    if bg.unlocked == false then
      love.graphics.draw(textures.bitcoin,backgroundmenu.x+10+24+160,24+(i-1)*130+dragging.y,0,0.125)
      love.graphics.print(tostring(bg.cost),backgroundmenu.x+10+24+200,24+4+(i-1)*130+dragging.y)
    end
  end
  for _,option in ipairs(options) do
    -- Name --
    love.graphics.print(option.name,optionsmenu.x+10,option.y)
    -- Left Arrow --
    love.graphics.draw(textures.arrow,optionsmenu.x+20+78,option.y+140,math.pi,0.5)
    -- Value --
    love.graphics.setFont(bigfont)
    if option.value < 10 then
      love.graphics.print(tostring(option.value),optionsmenu.x+138,option.y+66)
    elseif option.value >= 10 then
      love.graphics.print(tostring(option.value),optionsmenu.x+118,option.y+66)
    end
    love.graphics.setFont(smallfont)
    -- Right Arrow --
    love.graphics.draw(textures.arrow,optionsmenu.x+200,option.y+40,0,0.5)
  end
  love.graphics.setFont(upgradefont)
  for i,upgrade in ipairs(upgrades) do
    love.graphics.setColor(255,255,255,100)
    love.graphics.rectangle("fill",upgrade.x,upgrade.y,upgrade.width,upgrade.height,12,12)
    love.graphics.setColor(255,255,255)
    --love.graphics.print(upgrade.text,upgrade.x,upgrade.y)
    if upgrade.texture then
      if upgrade.scale then
        love.graphics.draw(upgrade.texture,upgrade.x+10,upgrade.y+8,0,upgrade.scale)
      else
        love.graphics.draw(upgrade.texture,upgrade.x+10,upgrade.y+8,0,1)
      end
      if upgrade.showplus then
        love.graphics.draw(textures.plus,upgrade.x+50,upgrade.y+20,0,0.8)
        love.graphics.draw(textures.plus,upgrade.x+60,upgrade.y+10,0,1)
      end
    end
    if upgrade.level and i ~= 2 and i ~= 4 then
      if upgrade.level < 10 then
        love.graphics.print("Level: " .. "  " .. upgrade.level,upgrade.x+upgrade.width-140,upgrade.y+upgrade.height-44)
      elseif upgrade.level >= 10 then
        love.graphics.print("Level: " .. upgrade.level,upgrade.x+upgrade.width-140,upgrade.y+upgrade.height-44)
      end
    elseif upgrade.level and i == 2 then
      love.graphics.print("Bombs: " .. upgrade.level,upgrade.x+upgrade.width-140,upgrade.y+upgrade.height-44)
    elseif upgrade.level and i == 4 then
      love.graphics.print("Lives:  " .. upgrade.level,upgrade.x+upgrade.width-140,upgrade.y+upgrade.height-44)
    end
    if upgrade.maxlevel then
      love.graphics.print("/" .. upgrade.maxlevel,upgrade.x+upgrade.width-140+90,upgrade.y+upgrade.height-44)
    end
    if upgrade.maxlevel and upgrade.level == upgrade.maxlevel then
      love.graphics.setColor(52,168,83)
      love.graphics.print("MAXIMUM",upgrade.x+upgrade.width-140,upgrade.y+upgrade.height-21)
      love.graphics.setColor(255,255,255)
    else
      if upgrade.cost > 1000000 then 
        love.graphics.print("Cost: " .. math.ceil(100*(upgrade.cost/1000000))*0.01 .. "m",upgrade.x+upgrade.width-140,upgrade.y+upgrade.height-21)
      else
        love.graphics.print("Cost: " .. upgrade.cost,upgrade.x+upgrade.width-140,upgrade.y+upgrade.height-21)
      end
    end
    if upgrade.stat == "bombbuy" and canbuybombtimer > 0 then
      love.graphics.setColor(0,0,0,100)
      love.graphics.rectangle("fill",upgrade.x,upgrade.y,upgrade.width,upgrade.height,12,12)
      love.graphics.setColor(234,67,53)
      love.graphics.setFont(timerfont)
      love.graphics.print(tostring(canbuybombtimer),upgrade.x+62,upgrade.y+10)
      love.graphics.setFont(upgradefont)
    end
    if upgrade.stat == "bombdamage" then
      love.graphics.print(tostring(bombdamage) .. "x",upgrade.x+48,upgrade.y+upgrade.height-21)
      love.graphics.setColor(255,255,255)
      love.graphics.rectangle("fill",upgrade.x+48+40,upgrade.y+upgrade.height-18,20,15)
      love.graphics.setColor(94,96,93)
      love.graphics.setLineWidth(3)
      love.graphics.rectangle("line",upgrade.x+48+40,upgrade.y+upgrade.height-18,20,15)
      
    end
  end
  love.graphics.print("Statistics",stats[1].x+88,8+dragging.y)
  love.graphics.setColor(255,255,255,255)
  love.graphics.setFont(smallfont)
  for _,stat in ipairs(stats) do
    love.graphics.print(stat.text .. ": " .. tostring(stat.value),stats[1].x+0,stat.y+20+dragging.y)
  end
  for i,achievement in ipairs(achievements) do
    love.graphics.setColor(255,255,255,255)
    if achievement.icon then
      if i <= 4 then
        love.graphics.draw(achievement.icon,stats[1].x+((i-1)*(64+8)),200+dragging.y,0,0.25)
      elseif i > 4 and i <= 8 then
        love.graphics.draw(achievement.icon,stats[1].x+((i-5)*(64+8)),272+dragging.y,0,0.25)
      elseif i > 8 and i <= 12 then
        love.graphics.draw(achievement.icon,stats[1].x+((i-9)*(64+8)),344+dragging.y,0,0.25)
      elseif i > 12 and i <= 16 then
        love.graphics.draw(achievement.icon,stats[1].x+((i-13)*(64+8)),416+dragging.y,0,0.25)
      elseif i > 16 and i <= 20 then
        love.graphics.draw(achievement.icon,stats[1].x+((i-17)*(64+8)),488+dragging.y,0,0.25)
      elseif i > 20 and i <= 24 then
        love.graphics.draw(achievement.icon,stats[1].x+((i-21)*(64+8)),560+dragging.y,0,0.25)
      elseif i > 24 and i <= 28 then
        love.graphics.draw(achievement.icon,stats[1].x+((i-25)*(64+8)),632+dragging.y,0,0.25)
      elseif i > 28 and i <= 32 then
        love.graphics.draw(achievement.icon,stats[1].x+((i-29)*(64+8)),704+dragging.y,0,0.25)
      elseif i > 32 and i <= 36 then
        love.graphics.draw(achievement.icon,stats[1].x+((i-33)*(64+8)),776+dragging.y,0,0.25)
      end
    end
    if achievement.completed == false then
      if i <= 4 then
        love.graphics.draw(textures.achievement_slot,stats[1].x+((i-1)*(64+8)),200+dragging.y,0,0.25)
      elseif i > 4 and i <= 8 then
        love.graphics.draw(textures.achievement_slot,stats[1].x+((i-5)*(64+8)),272+dragging.y,0,0.25)
      elseif i > 8 and i <= 12 then
        love.graphics.draw(textures.achievement_slot,stats[1].x+((i-9)*(64+8)),344+dragging.y,0,0.25)
      elseif i > 12 and i <= 16 then
        love.graphics.draw(textures.achievement_slot,stats[1].x+((i-13)*(64+8)),416+dragging.y,0,0.25)
      elseif i > 16 and i <= 20 then
        love.graphics.draw(textures.achievement_slot,stats[1].x+((i-17)*(64+8)),488+dragging.y,0,0.25)
      elseif i > 20 and i <= 24 then
        love.graphics.draw(textures.achievement_slot,stats[1].x+((i-21)*(64+8)),560+dragging.y,0,0.25)
      elseif i > 24 and i <= 28 then
        love.graphics.draw(textures.achievement_slot,stats[1].x+((i-25)*(64+8)),632+dragging.y,0,0.25)
      elseif i > 28 and i <= 32 then
        love.graphics.draw(textures.achievement_slot,stats[1].x+((i-29)*(64+8)),704+dragging.y,0,0.25)
      elseif i > 32 and i <= 36 then
        love.graphics.draw(textures.achievement_slot,stats[1].x+((i-33)*(64+8)),776+dragging.y,0,0.25)
      end
    end
  end
  if quickmenu.opened == true then
    for _,quickmenuit in ipairs(quickmenuitems) do
      love.graphics.setColor(255,255,255)
      love.graphics.draw(quickmenuit.icon,quickmenuit.x,quickmenuit.y,0,0.25,0.25)
    end
  end
end
return upgradeClass