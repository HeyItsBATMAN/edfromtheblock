local popupClass = {}
bossactive = false
function popupClass.generate(spawnboss)
    hp=love.math.random(2,4)
  if spawnboss and spawnboss == true and stage >= 5 then
    pw = 800
    ph = 600
    hp=stage * 2
    newpopup = {dw="fill",x=((wwidth-pw)/2),y=((wheight-ph)/2),width=pw+2,height=ph,hp=hp,rhp=hp,canvas=love.graphics.newCanvas(pw+16,ph+63),canvasscale=0,number=1,boss=true,transparency=1,closed=false}
    table.insert(popups,newpopup)
    popupamount = 0
    bossactive = true
    timer.cancel(timers.gentimer)
  elseif bossactive == false then
    popupamount = 0
    for _,_ in ipairs(texpopups) do
      popupamount = popupamount + 1
    end
    getp = math.random(1,popupamount)
    pw = texpopups[getp].width
    ph = texpopups[getp].height
    if getp == 12 then
      if love.math.random(1,2) == 2 then
        newpopup = {dw="fill",x=love.math.random(40,wwidth-(pw*texpopups[getp].scalex)-40),y=0-ph,width=(pw+2)*texpopups[getp].scalex,height=ph*texpopups[getp].scaley,hp=1,rhp=1,canvas=love.graphics.newCanvas((pw+16)*texpopups[getp].scalex,(ph+63)*texpopups[getp].scaley),canvasscale=0,number=getp,transparency=1,closed=false,draggable=false,malware=true,malwaremoved=false}
      else
        newpopup = {dw="fill",x=love.math.random(40,wwidth-(pw*texpopups[getp].scalex)-40),y=love.graphics.getHeight(),width=(pw+2)*texpopups[getp].scalex,height=ph*texpopups[getp].scaley,hp=1,rhp=1,canvas=love.graphics.newCanvas((pw+16)*texpopups[getp].scalex,(ph+63)*texpopups[getp].scaley),canvasscale=0,number=getp,transparency=1,closed=false,draggable=false,malware=true,malwaremoved=false}
      end
    elseif getp == 13 then
      if love.math.random(1,3+math.floor(stage/10)) == 3 then
        newpopup = {dw="fill",x=love.math.random(40,wwidth-(pw*texpopups[getp].scalex)-40),y=love.math.random(120,wheight-(ph*texpopups[getp].scaley)-120),width=(pw+2)*texpopups[getp].scalex,height=ph*texpopups[getp].scaley,hp=1,rhp=1,canvas=love.graphics.newCanvas((pw+16)*texpopups[getp].scalex,(ph+63)*texpopups[getp].scaley),canvasscale=0,number=getp,transparency=1,closed=false,draggable=false,gold=true}
        timer.after(5,function ()
              for i,popup in ipairs(popups) do
                if popup.gold and popup.gold == true then
                  table.remove(popups,i)
                end
              end
            end)
        gold.spawn:stop()
        gold.spawn:play()
      else popupClass.generate() return end
    else
    if love.math.random(1,2) == 2 then
      newpopup = {dw="fill",x=love.math.random(40,wwidth-(pw*texpopups[getp].scalex)-40),y=love.math.random(120,wheight-(ph*texpopups[getp].scaley)-120),width=(pw+2)*texpopups[getp].scalex,height=ph*texpopups[getp].scaley,hp=1,rhp=1,canvas=love.graphics.newCanvas((pw+16)*texpopups[getp].scalex,(ph+63)*texpopups[getp].scaley),canvasscale=0,number=getp,transparency=1,closed=false,dragging=false,draggable=true}
    else
      newpopup = {dw="fill",x=love.math.random(40,wwidth-(pw*texpopups[getp].scalex)-40),y=love.math.random(120,wheight-(ph*texpopups[getp].scaley)-120),width=(pw+2)*texpopups[getp].scalex,height=ph*texpopups[getp].scaley,hp=hp,rhp=hp,canvas=love.graphics.newCanvas((pw+16)*texpopups[getp].scalex,(ph+63)*texpopups[getp].scaley),canvasscale=0,number=getp,transparency=1,closed=false,draggable=false}
    end
    end
    table.insert(popups,newpopup)
    popupamount = 0
  end
  return
end
function popupClass.animate()
  if texpopups then
  for _,texpopup in ipairs(texpopups) do
    if texpopup.currentframe < texpopup.frames then
      texpopup.currentframe = texpopup.currentframe + 1
    elseif texpopup.currentframe == texpopup.frames then
      texpopup.currentframe = 1
    end
  end
  end
end
function popupClass.draw()
  for i, popup in ipairs(popups) do
    if i > (#popups - 10) then
    transparencyShader:send("opacity",popup.transparency/150)
    love.graphics.setCanvas(popup.canvas)
    if popup.draggable == true then
      love.graphics.setColor(41,183,26,popup.transparency)
    elseif popup.malware == true then
      love.graphics.setColor(200,30,30,popup.transparency)
    elseif popup.gold == true then
      love.graphics.setColor(218,165,32,popup.transparency)
    else
      love.graphics.setColor(94,96,93,popup.transparency)
    end
    love.graphics.rectangle("fill",0,0,popup.width+16,popup.height+63)
    love.graphics.setColor(255,255,255,225)
    love.graphics.draw(texpopups[popup.number].text,popup.width/2-(texpopups[popup.number].text:getWidth())/2,2)
    if popup.hp > 1 then
      for i=popup.rhp,1,-1 do
        if popup.hp == 2 then
          if popup.rhp == 2 then
            love.graphics.setColor(6,89,7)
          elseif popup.rhp == 1 then
            love.graphics.setColor(203,5,4)
          end
        elseif popup.hp == 3 then
          if popup.rhp == 3 then
            love.graphics.setColor(6,89,7)
          elseif popup.rhp == 2 then
            love.graphics.setColor(249,216,27)
          elseif popup.rhp == 1 then
            love.graphics.setColor(203,5,4)
          end
        elseif popup.hp == 4 then
          if popup.rhp == 4 then
            love.graphics.setColor(6,89,7)
          elseif popup.rhp == 3 then
            love.graphics.setColor(249,216,27)
          elseif popup.rhp == 2 then
            love.graphics.setColor(223,74,27)
          elseif popup.rhp == 1 then
            love.graphics.setColor(203,5,4)
          end
        end
        love.graphics.rectangle("fill",popup.width-(14*i*windowscalex)-5,5,14*windowscalex,14*windowscaley)
        love.graphics.setColor(255,255,255)
        love.graphics.setLineWidth(2)
        love.graphics.rectangle("line",popup.width-(14*i*windowscalex)-5,5,14*windowscalex,14*windowscaley)
      end 
    end
    love.graphics.setShader(transparencyShader)
    love.graphics.setCanvas()
    love.graphics.draw(popup.canvas,popup.x,popup.y,0,1,popup.canvasscale)
    love.graphics.setCanvas(texpopups[popup.number].canvas)
    love.graphics.setColor(255,255,255,popup.transparency)
    love.graphics.draw(texpopups[popup.number].frame[texpopups[popup.number].currentframe],0,0)
    love.graphics.setCanvas()
    love.graphics.draw(texpopups[popup.number].canvas,popup.x+8*texpopups[popup.number].scalex,popup.y+55*texpopups[popup.number].scaley,0,texpopups[popup.number].scalex,popup.canvasscale*texpopups[popup.number].scaley)
    love.graphics.setShader()
    --love.graphics.setColor(0,0,0,(11-i)*25.5)
    --love.graphics.rectangle("fill",popup.x,popup.y,popup.width+16,popup.height+63)
    love.graphics.setColor(255,255,255)
    end
  end
end
return popupClass