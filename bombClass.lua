bombClass = {}
bombdamage = 2
canbuybomb = true
canbuybombtimer = 0
destroyedbybomb = 0
local mouseDecayClass = require("mouseDecayClass")
function bombClass.use(x,y)
  destroyedbybomb = 0
  if bomb >= 1 then
    bombsound:stop()
    bombsound:play()
    bomb = bomb -1
    upgrades[3].level = bomb
    upgrades[3].cost = math.floor(upgrades[3].basecost * math.pow(upgrades[3].multiplier,upgrades[3].level))
    --[[
    for i,popup in ipairs(popups) do
    popup.rhp = popup.rhp - bombdamage
    if popup.rhp <= 0 then
      flux.to(popups[i],0.2,{transparency=0})
      flux.to(popups[i],5,{canvasscale=0})
      highscore = highscore + 2*combomult
      bitcoins = math.ceil(bitcoins + (2*combomult)/5)
      addedcoins = math.ceil((2*combomult)/5)
      combo.combosuccess = 255
      combotextdecay = 255
      combomult = combomult + 2 + comboupgrade
      totalclosed=totalclosed + 1
      totalearned = totalearned + addedcoins
      coin[love.math.random(1,2)]:play()
      popupclosed = true
      destroyedbybomb = destroyedbybomb + 1
      if popups[i].boss and popups[i].boss == true then
        bossactive = false
      end
    end
  end]]--
  for i=0,bombdamage-1,1 do
    if popups and popups[(#popups)-i] and popups[(#popups)-i].rhp then
      popups[(#popups)-i].rhp = 0
      destroyedbybomb = destroyedbybomb + 1
    end
  end

  
  if destroyedbybomb > 0 then
      mouseDecayClass.add(wwidth*1/15,bitcoinpos.y-20,true)
    end
  end
  for i,achievement in ipairs(achievements) do
    if achievement.bombs then
      if destroyedbybomb >= achievement.bombs and achievement.completed == false then
        love.filesystem.write(completedachievements[i]:getFilename(),"true")
        achievement.completed = true
        flux.to(achievement,0.5,{transparency=255})
        if quickmenu.opened == true then 
        flux.to(achievement,0.5,{x=wwidth-200})
      else
        flux.to(achievement,0.5,{x=wwidth-412})
      end
      end
    end
  end
end
function bombClass.draw()
  love.graphics.draw(combotext,wwidth*1/15,bitcoinpos.y-200)
  
end
return bombClass