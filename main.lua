jit.on()

textures = {bitcoin=love.graphics.newImage("textures/misc/bitcoin.png",{mipmaps=true}),download=love.graphics.newImage("textures/misc/tacho_mit_zeiger.png",{mipmaps=true}),highscore=love.graphics.newImage("textures/misc/pokal.png",{mipmaps=true}),arrow=love.graphics.newImage("textures/misc/arrow.png",{mipmaps=true}),warning=love.graphics.newImage("textures/misc/warning.png",{mipmaps=true}),bomb=love.graphics.newImage("textures/icons/bomb.png",{mipmaps=true}),ramup=love.graphics.newImage("textures/misc/ramup.png",{mipmaps=true}),plus=love.graphics.newImage("textures/misc/plus.png",{mipmaps=true}),achievement_slot=love.graphics.newImage("textures/icons/achievements/achievement_slot.png"),combo_icon=love.graphics.newImage("textures/misc/combo_icon.png",{mipmaps=true}),kopf=love.graphics.newImage("textures/misc/kopf.png",{mipmaps=true}),multiclicker=love.graphics.newImage("textures/misc/multiclick_icon.png",{mipmaps=true})}
for _,texture in ipairs(textures) do
  texture:setMipmapFilter("nearest",1)
end

flux = require 'libs/flux'
timer = require 'libs/timer'
binser = require 'libs/binser'
local popupClass = require("popupClass")
local mouseDecayClass = require("mouseDecayClass")
local achievementClass = require("achievementClass")
local upgradeClass = require("upgradeClass")
local soundClass = require("soundClass")
local bombClass = require("bombClass")
local tutorialClass = require("tutorialClass")

function math.clamp(low, n, high) return math.min(math.max(low, n), high) end

completedachievements = {}
for i,achievement in ipairs(achievements) do
    newcompletedachievement = love.filesystem.newFile("achievement" .. tostring(i))
    table.insert(completedachievements,newcompletedachievement)
end

for i,compachieve in ipairs(completedachievements) do
  if love.filesystem.exists(compachieve:getFilename()) == false then
    love.filesystem.write(compachieve:getFilename(),"false")
  elseif love.filesystem.exists(compachieve:getFilename()) == true then
    if compachieve:read() == "true" then
      achievements[i].completed = true
    end
  end
end

function initPopups()
  texpopups = {
    {file="textures/popups/aegypten/aegypten",frame={},width=1082,height=664,scalex=0.4*windowscalex,scaley=0.4*windowscaley,text=love.graphics.newText(popupfont, "Ed Explorer - Egyptians"),frames=4,currentframe=1,canvas = love.graphics.newCanvas(1082,644)}
    ,{file="textures/popups/burger/burger",frame={},width=1086,height=646,scalex=0.4*windowscalex,scaley=0.4*windowscaley,text=love.graphics.newText(popupfont, "Ed Explorer - Delicious Burger"),frames=2,currentframe=1,canvas = love.graphics.newCanvas(1086,646)}
    ,{file="textures/popups/business/business",frame={},width=1084,height=647,scalex=0.4*windowscalex,scaley=0.4*windowscaley,text=love.graphics.newText(popupfont, "Ed Explorer - Business"),frames=2,currentframe=1,canvas = love.graphics.newCanvas(1084,647)}
    ,{file="textures/popups/cutecats/cutecats",frame={},width=561,height=334,scalex=0.8*windowscalex,scaley=0.8*windowscaley,text=love.graphics.newText(popupfont, "Ed Explorer - Cute Cats"),frames=2,currentframe=1,canvas = love.graphics.newCanvas(561,334)}
    ,{file="textures/popups/Fluffydog",frame={},width=562,height=334,scalex=0.7*windowscalex,scaley=0.7*windowscaley,text=love.graphics.newText(popupfont, "Ed Explorer - Fluffy Dog"),frames=1,currentframe=1,canvas = love.graphics.newCanvas(562,334)}
    ,{file="textures/popups/goshopping/goshopping",frame={},width=1081,height=643,scalex=0.55*windowscalex,scaley=0.55*windowscaley,text=love.graphics.newText(popupfont, "Ed Explorer - Go Shopping"),frames=2,currentframe=1,canvas = love.graphics.newCanvas(1081,643)}
    ,{file="textures/popups/happyfamily/happyfamily",frame={},width=1086,height=649,scalex=0.45*windowscalex,scaley=0.45*windowscaley,text=love.graphics.newText(popupfont, "Ed Explorer - Happy Family"),frames=2,currentframe=1,canvas = love.graphics.newCanvas(1086,649)}
    ,{file="textures/popups/iceworld/iceworld",frame={},width=1036,height=644,scalex=0.45*windowscalex,scaley=0.45*windowscaley,text=love.graphics.newText(popupfont, "Ed Explorer - Ice World"),frames=2,currentframe=1,canvas = love.graphics.newCanvas(1036,644)}
    ,{file="textures/popups/PandaFarm",frame={},width=561,height=334,scalex=0.75*windowscalex,scaley=0.75*windowscaley,text=love.graphics.newText(popupfont, "Ed Explorer - Panda Farm"),frames=1,currentframe=1,canvas = love.graphics.newCanvas(561,334)}
    ,{file="textures/popups/Sandwich",frame={},width=1082,height=644,scalex=0.45*windowscalex,scaley=0.45*windowscaley,text=love.graphics.newText(popupfont, "Ed Explorer - Sandwich"),frames=1,currentframe=1,canvas = love.graphics.newCanvas(1082,644)}
    ,{file="textures/popups/sushininja/sushininja",frame={},width=1081,height=644,scalex=0.45*windowscalex,scaley=0.45*windowscaley,text=love.graphics.newText(popupfont, "Ed Explorer - Sushi Ninja"),frames=2,currentframe=1,canvas = love.graphics.newCanvas(1081,644)}
    ,{file="textures/popups/malwaredetected",frame={},width=1098,height=707,scalex=0.5*windowscalex,scaley=0.5*windowscaley,text=love.graphics.newText(popupfont, "Ed Explorer - Malware Detected"),frames=1,currentframe=1,canvas = love.graphics.newCanvas(1098,707)}
    ,{file="textures/popups/gold/gold",frame={},width=743,height=480,scalex=0.5*windowscalex,scaley=0.5*windowscaley,text=love.graphics.newText(popupfont, "Ed Explorer - Gold"),frames=11,currentframe=1,canvas = love.graphics.newCanvas(743,480)}
    ,{file="textures/popups/hotdog/hotdog",frame={},width=1082,height=644,scalex=0.5*windowscalex,scaley=0.5*windowscaley,text=love.graphics.newText(popupfont, "Ed Explorer - Hot German Sausage"),frames=26,currentframe=1,canvas = love.graphics.newCanvas(1082,644)}
    ,{file="textures/popups/papagei/papagei",frame={},width=1098,height=707,scalex=0.5*windowscalex,scaley=0.5*windowscaley,text=love.graphics.newText(popupfont, "Ed Explorer - Zoo"),frames=2,currentframe=1,canvas = love.graphics.newCanvas(1098,707)}
    ,{file="textures/popups/savetheelephants/savetheelephants",frame={},width=1280,height=280,scalex=0.6*windowscalex,scaley=0.6*windowscaley,text=love.graphics.newText(popupfont, "Ed Explorer - Save The Elephants"),frames=14,currentframe=1,canvas = love.graphics.newCanvas(1280,280)}
    ,{file="textures/popups/vacation/vacation",frame={},width=1098,height=707,scalex=0.5*windowscalex,scaley=0.5*windowscaley,text=love.graphics.newText(popupfont, "Ed Explorer - Vacation"),frames=2,currentframe=1,canvas = love.graphics.newCanvas(1098,707)}
    ,{file="textures/popups/wetcat/wetcat",frame={},width=550,height=327,scalex=0.9*windowscalex,scaley=0.9*windowscaley,text=love.graphics.newText(popupfont, "Ed Explorer - Wet Pussy"),frames=4,currentframe=1,canvas = love.graphics.newCanvas(550,327)}
    }
  for _,texpopup in ipairs(texpopups) do
    for v=1,texpopup.frames,1 do
      texpopup.frame[v]=love.graphics.newImage(texpopup.file .. " (" .. v .. ").png")
    end
  end
end

function love.load()
  gamepaused = false
  volume = 1
  love.audio.setVolume(volume)
  bitcoinsave = love.filesystem.newFile("bitcoinsave")
  highscoresave = love.filesystem.newFile("highscoresave")
  combomultsave = love.filesystem.newFile("combomultsave")
  stagesave = love.filesystem.newFile("stagesave")
  totalclosedsave = love.filesystem.newFile("totalclosedsave")
  livessave = love.filesystem.newFile("livessave")
  upgradebombsave = love.filesystem.newFile("upgradebomb")
  upgrademultisave = love.filesystem.newFile("upgrademulti")
  globalbitcoins = love.filesystem.newFile("globalbitcoins")
  globalhighscore = love.filesystem.newFile("globalhighscore")
  if love.filesystem.exists("introplayed") == false then
    introplayedfile = love.filesystem.newFile("introplayed")
  end
  if love.filesystem.exists("tutorialcompleted") == false then
    tutorialcompletedfile = love.filesystem.newFile("tutorialcompleted")
  end
  if love.filesystem.read("introplayed") and love.filesystem.read("introplayed") == "true" then
    introplayed = true
  else
    introplayed = false
    introvideo = love.graphics.newVideo("edblock.ogg",true)
  end
  if love.filesystem.read("tutorialcompleted") and love.filesystem.read("tutorialcompleted") == "true" then
    tutorialcompleted = true
  else
    tutorialcompleted = false
  end
  introtextopacity = {op = 0}
  operatingsystem = love.system.getOS()
  lives = 3
  
  -- FPS CAP Part 1 --
  if operatingsystem == "Android" then
    min_dt = 1/30
  else
    min_dt = 1/60
  end
  next_time = love.timer.getTime()
  -- FPS CAP Part 1 Ende --
  
  --bigfont = love.graphics.newFont("MunroSmall.ttf",100)
  --smallfont = love.graphics.newFont("MunroSmall.ttf",26)
  --bigfont = love.graphics.newFont("Unibody8Pro-Regular.otf",50)
  --smallfont = love.graphics.newFont("Unibody8Pro-Regular.otf",16)
  --upgradefont = love.graphics.newFont("Unibody8Pro-Regular.otf",14)
  wwidth,wheight = love.graphics.getDimensions()
  windowscalex = wwidth/1280
  windowscaley = wheight/720
  
  bigfont = love.graphics.newFont("riffic.otf",50)
  hugefont = love.graphics.newFont("riffic.otf",160)
  timerfont = love.graphics.newFont("riffic.otf",32)
  if windowscalex < 1 then
    bigfont = love.graphics.newFont("riffic.otf",30)
  elseif windowscalex >= 1 then
    bigfont = love.graphics.newFont("riffic.otf",50)
  end
  if windowscalex < 1 then
    smallfont = love.graphics.newFont("riffic.otf",16)
  elseif windowscalex >= 1 then
    smallfont = love.graphics.newFont("riffic.otf",24)
  end
  if windowscalex < 1 then
    popupfont = love.graphics.newFont("riffic.otf",14)
  elseif windowscalex >= 1 then
    popupfont = love.graphics.newFont("riffic.otf",18)
  end
  upgradefont = love.graphics.newFont("riffic.otf",20)
  
  menuButton = {x=wwidth-150,y=wheight*8/9+6,width=90,height=25}
  statusbar = love.graphics.newCanvas(800,300)
  -- Shaders --
  transparencyShader = love.graphics.newShader[[
extern number opacity;
vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords ){
  vec4 pixel = Texel(texture, texture_coords );
  return vec4(pixel.r,pixel.g,pixel.b,opacity);
}]]
  --
  
  totalearned = 0
  addedcoins = 0
  popups = {}
  popupcount = 0
  mousedecaycount = 0
  globals = {highscore = 0, bitcoins = 0}
  if love.filesystem.exists(highscoresave:getFilename()) == true then
    local readhighscore = love.filesystem.read(highscoresave:getFilename())
    highscore = tonumber(readhighscore)
    lasthighscore = tonumber(readhighscore)
  else
    highscore = 0
    lasthighscore = 0
  end
  if love.filesystem.exists(bitcoinsave:getFilename()) == true then
    local readbitcoin = love.filesystem.read(bitcoinsave:getFilename())
    bitcoins = tonumber(readbitcoin)
    lastbit = tonumber(readbitcoin)
  else
    bitcoins = 0
    lastbit = 0
  end
  if love.filesystem.exists(combomultsave:getFilename()) == true then
    local readcombomult = love.filesystem.read(combomultsave:getFilename())
    combomult = tonumber(readcombomult)
    lastcombo = tonumber(readcombomult)
  else
    combomult = 1
    lastcombo = 1
  end
  if love.filesystem.exists(stagesave:getFilename()) == true then
    local readstage = love.filesystem.read(stagesave:getFilename())
    stage = tonumber(readstage)
    laststage = tonumber(readstage)
  else
    stage = 1
    laststage = 1
  end
  if love.filesystem.exists(totalclosedsave:getFilename()) == true then
    local readtotalclosed = love.filesystem.read(totalclosedsave:getFilename())
    totalclosed =tonumber(readtotalclosed)
  else
    totalclosed = 0
  end
  if love.filesystem.exists(globalbitcoins:getFilename()) == true then
    local readglobalbitcoins = love.filesystem.read(globalbitcoins:getFilename())
    globals.bitcoins =tonumber(readglobalbitcoins)
  else
    globals.bitcoins = 0
  end
  if love.filesystem.exists(globalhighscore:getFilename()) == true then
    local readglobalhighscore = love.filesystem.read(globalhighscore:getFilename())
    globals.highscore =tonumber(readglobalhighscore)
  else
    globals.highscore = 0
  end
  if love.filesystem.exists(livessave:getFilename()) == true then
    local readlivessave = love.filesystem.read(livessave:getFilename())
    lives =tonumber(readlivessave)
    if lives == 0 then
      lives = 3
    end
  else
    lives = 3
  end
  if love.filesystem.exists(upgradebombsave:getFilename()) == true then
    local readupgradebombsave = love.filesystem.read(upgradebombsave:getFilename())
    upgrades[3].level =tonumber(readupgradebombsave)
  end
  if love.filesystem.exists(upgrademultisave:getFilename()) == true then
    local readupgrademultisave = love.filesystem.read(upgrademultisave:getFilename())
    upgrades[5].level =tonumber(readupgrademultisave)
  end
  if introplayed == true and tutorialcompleted == true then
    initPopups()
  end
  brightness = 4
  --timer.after(2,function() bitcoins = 100000 end)
  hp = 0
  maxpopups = 10
  comboupgrade = 0
  popupclosed = false
  closedbydrag = false
  clickedmalware = false
  gentimerinterval = 60/(20+stage)
  bomb = 3
  love.graphics.setLineWidth(3)
  love.mouse.setVisible(false)
  tutorials = {love.graphics.newImage("textures/tutorial/tutorial_1.png"),love.graphics.newImage("textures/tutorial/tutorial_2.png"),love.graphics.newImage("textures/tutorial/tutorial_3.png"),love.graphics.newImage("textures/tutorial/tutorial_4.png")}
  backgrounds = {wiese=love.graphics.newImage("textures/backgrounds/wiese.png"),colorful_sands=love.graphics.newImage("textures/backgrounds/colorful_sands.png"),africa=love.graphics.newImage("textures/backgrounds/africa.png"),alpine=love.graphics.newImage("textures/backgrounds/alpine.png"),snowflakes=love.graphics.newImage("textures/backgrounds/snowflakes.png")}
  activebackground = 2
  
  bluescreens = {love.graphics.newImage("textures/bluescreens/bluescreen.png")}
  bluescreen = false
  nextbluescreen = 1
  selection = {{x=wwidth-100,y=wheight-40},{x=wwidth-100,y=wheight-20}}
  selectionindex = 1
  speedup = 1
  speedupcost = 1
  ramup = 1
  ramupcost = 1
  seed = os.time()
  math.randomseed(seed)
  highestcombo = 0
  totalclicked = 0
  lastboss = 0
  combo = {combofail = 0,combosuccess = 0}
  dragarea = {x=0,y=0,width=love.graphics.getWidth(),height=50,opacity=0}
  
  mousedecay = {}
  
  debugtext = love.graphics.newCanvas(400,300)
  combotext = love.graphics.newCanvas(wwidth/3,wheight/3)
  bitcoinpos = {x=wwidth*4/9,y=wheight*9/10}
  coinarea = {x=wwidth*4/9,y=wheight*9/1,opacity=0}
  higharea = {x=wwidth*4/9,y=wheight*9/1,opacity=0}
  comboarea = {x=wwidth*4/9,y=wheight*9/1,opacity=0}
  --popupClass.generate()
  timers = {gentimer=timer.every(gentimerinterval,function () popupClass.generate() end),animtimer=timer.every(0.2,function () popupClass.animate() end),goldtimer = timer.during(77,function () goldcombo = 4 end, function () goldcombo = 0 end),goldtimertime = 0,goldtimertimetimer = timer.every(0.1,function () timers.goldtimertime = timers.goldtimertime - 1 end, 770)}
  upgradeClass.openMenu()
  upgradeClass.updateOptions({brightness,volume*10})
  upgrades[2].level = bomb
  upgrades[2].cost = math.floor(upgrades[2].basecost * math.pow(upgrades[2].multiplier,upgrades[2].level))
  stagesplash = {x=-wwidth}
  goldcombo = 0
  timer.cancel(timers.gentimer)
  timer.cancel(timers.goldtimer)
  timer.cancel(timers.goldtimertimetimer)   
  
  
  if introplayed == true and tutorialcompleted == true then
    timers.gentimer = timer.every(gentimerinterval,function () popupClass.generate() end)
  end
  
end

function love.focus(f)
  if f == false then
    gamepaused = true
    love.audio.setVolume(0)
    love.filesystem.write(bitcoinsave:getFilename(),bitcoins)
    love.filesystem.write(highscoresave:getFilename(),highscore)
    love.filesystem.write(combomultsave:getFilename(),combomult)
    love.filesystem.write(stagesave:getFilename(),stage)
    love.filesystem.write(totalclosedsave:getFilename(),totalclosed)
    love.filesystem.write(globalbitcoins:getFilename(),globals.bitcoins)
    love.filesystem.write(globalhighscore:getFilename(),globals.highscore)
    love.filesystem.write(livessave:getFilename(),lives)
    love.filesystem.write(upgradebombsave:getFilename(),upgrades[3].level)
    love.filesystem.write(upgrademultisave:getFilename(),upgrades[5].level)
    themesong:stop()
  else
    gamepaused = false
    love.audio.setVolume(volume)
  end
end

function resetStats()
  love.filesystem.write(bitcoinsave:getFilename(),0)
  bitcoins = 0
  love.filesystem.write(highscoresave:getFilename(),0)
  highscore = 0
  love.filesystem.write(combomultsave:getFilename(),1)
  combomult = 1
  love.filesystem.write(stagesave:getFilename(),1)
  stage = 1
  love.filesystem.write(totalclosedsave:getFilename(),0)
  totalclosed = 0
  love.filesystem.write(upgradebombsave:getFilename(),0)
  upgrades[3].level = 0
  love.filesystem.write(upgrademultisave:getFilename(),0)
  upgrades[5].level = 0
  gentimerinterval = 60/(20+stage)
end

function love.quit()
  love.filesystem.write(bitcoinsave:getFilename(),bitcoins)
  love.filesystem.write(highscoresave:getFilename(),highscore)
  love.filesystem.write(combomultsave:getFilename(),combomult)
  love.filesystem.write(stagesave:getFilename(),stage)
  love.filesystem.write(totalclosedsave:getFilename(),totalclosed)
  love.filesystem.write(globalbitcoins:getFilename(),globals.bitcoins)
  love.filesystem.write(globalhighscore:getFilename(),globals.highscore)
  love.filesystem.write(livessave:getFilename(),lives)
  love.filesystem.write(upgradebombsave:getFilename(),upgrades[3].level)
  love.filesystem.write(upgrademultisave:getFilename(),upgrades[5].level)
end

function popupClick(x,y,button)
  if button == 1 and x > quickmenu.x then
      if quickmenu.opened == true or quickmenuitems[1].opened == true or quickmenuitems[2].opened == true or quickmenuitems[3].opened == true or quickmenuitems[4].opened == true then 
        upgradeClass.click(x,y)
      end
  elseif button == 1 and x > wwidth*1/15-30 and x < wwidth*1/15+ 128 and y > bitcoinpos.y-60 and y < wheight then
    bombClass.use(x,y)
  else
      for i=popupcount,1,-1 do
      if popups[i] then
        if button == 1 and popups[i].draggable == false and x > popups[i].x and x < popups[i].x+popups[i].width and y > popups[i].y and y < popups[i].y+popups[i].height+24 and popups[i].transparency > 100 then
        
        if popups[i].malware == true then
          clickedmalware = true
          popups[i].rhp = 0
          combomult = 1
          mouseDecayClass.add(x,y,true)
          return
        elseif popups[i].gold == true then
          timer.cancel(timers.goldtimer)
          timer.cancel(timers.goldtimertimetimer)
          timers.goldtimer = timer.during(77,function () goldcombo = 4 end, function () goldcombo = 0 end)
          timers.goldtimertime = 770
          timers.goldtimertimetimer = timer.every(0.1,function () timers.goldtimertime = timers.goldtimertime - 1 end, 770)
          gold.click:stop()
          gold.click:play()
        end
        if popups[i].rhp > 1 then
          popups[i].rhp = popups[i].rhp - (1+upgrades[5].level)
        elseif popups[i].rhp <= 1 then
          popups[i].rhp = 0
          mouseDecayClass.add(x,y,true)
        end
        popupclick:stop()
        popupclick:play()
        popups[i].dragging = true
        
        
        popupclosed = true
        totalclicked = totalclicked + 1
        if highestcombo < combomult then
          highestcombo = combomult
        end
        return
      end
        end
      end
      if button == 1 and popupclosed == false and closedbydrag == false and bluescreen == false then
      lives = lives - 1
      combomult = 1
      combo.combofail = 255
      combotextdecay = 255
      combo.combosuccess = 0
      mouseDecayClass.add(x,y)
      miss[1]:stop()
      miss[2]:stop()
      if lives > 0 then
        miss[love.math.random(1,2)]:play()
      end
      if operatingsystem == 'Android' then
        love.system.vibrate(0.2)
      end
      return
    end
  end
end
function love.mousepressed(x,y,button)
  if x > quickmenu.x then
      dragging.active = true
      dragging.diffY = y - dragging.y
  end
  for i=popupcount,1,-1 do
      if popups[i] and tutorialcompleted == true then
        if button == 1 and x > popups[i].x and x < popups[i].x+popups[i].width and y > popups[i].y and y < popups[i].y+popups[i].height+24 and popups[i].transparency > 100 and popups[i].draggable == true then
          popups[i].dragging = true
          closedbydrag = true
          return
        end
      end
  end
end
function love.mousereleased(x, y, button)
  if introplayed == false and introvideo:tell() > 5 then
    introvideo:pause()
    introplayed = true
    introvideo = nil
    love.filesystem.write(introplayedfile:getFilename(),"true")
    initPopups()
  elseif tutorialcompleted == false and introplayed == true then
    tutorialClass.click(x,y)
  elseif tutorialcompleted == true and introplayed == true then
    if love.graphics.getWidth() and love.graphics.getHeight() then
        if x and y then
            popupClick(x,y,button)
        end
    end
  end
  dragging.active = false
  for _,popup in ipairs(popups) do
    popup.dragging = false
  end
end
function love.update(dt)
  --require("libs/lovebird").clear() 
  --print(collectgarbage('count'))
  --require("libs/lovebird").update() 
  if gamepaused == false then
  upgradeClass.update()
  upgradeClass.updateStats({totalclicked,totalearned,totalclosed,highestcombo,stage,globals.bitcoins,globals.highscore,lives})
  timer.update(dt)
  flux.update(dt)
  achievementClass.update(dt)
  
   
  if introplayed == false and introvideo:isPlaying() and introvideo:tell() > 41 then
    introvideo:pause()
    introplayed = true
    introvideo = nil
    initPopups()
    love.filesystem.write(introplayedfile:getFilename(),"true")
    timers.gentimer=timer.every(gentimerinterval,function () popupClass.generate() end)
  elseif introplayed == false and not introvideo:isPlaying() and gamepaused == false then
    introvideo:play()
    flux.to(introtextopacity,1,{op=255}):delay(4)
  elseif introplayed == true and gamepaused == false then
  if tutorialcompleted == false then
    timer.cancel(timers.gentimer)
  end
  
  love.audio.setVolume(volume)
  if bluescreen == false and gamepaused == false then
    soundClass.playTheme()
  end
  if lives == 0 and bluescreen == false then
    for i in pairs(popups) do
    popups[i] = nil
    end
    combomult = 1
    combo.combofail = 255
    combo.combosuccess = 0
    themesong:stop()
    bluescreensound:play()
    bluescreen = true
    nextbluescreen = love.math.random(1,1)
    lives = 0
    timer.cancel(timers.gentimer)
    resetStats()
    timer.after(5,function ()
        lives = 3
        bluescreen = false 
        timers.gentimer = timer.every(gentimerinterval,function () popupClass.generate() end)
        end)
  end
  -- FPS CAP Part 2 --
  next_time = next_time + min_dt
  -- FPS CAP ENDE Part 2 --
  
  -- Dragarea blink --
  if dragarea.opacity == 0 and closedbydrag == true then
    flux.to(dragarea,0.5,{opacity=80})
  elseif dragarea.opacity > 1 and closedbydrag == false then
    flux.to(dragarea,0.5,{opacity=0})
  end
  
  -- Coinarea blink --
  if bitcoins ~= lastbit and coinarea.opacity < 1 then
    coinarea.opacity = 255
  end
  if highscore ~= lasthighscore and higharea.opacity < 1 then
    higharea.opacity = 255
  end
  if combomult ~= lastcombo and comboarea.opacity < 1 then
    comboarea.opacity = 255
  end
  
  if dragging.active == true and dragging.y <= 5 then
    dragging.y = love.mouse.getY() - dragging.diffY
  elseif dragging.y > 5 then
    dragging.y = dragging.y-5
  elseif dragging.y < -260-(720-wheight) then
    dragging.y = dragging.y+5
  end
  -- PopUpCount Variable zurücksetzen -- 
  popupcount = 0
  mousedecaycount = 0
  -- PopUpCount als Größe des Arrays setzen -- 
  for _, _ in ipairs(popups) do
    popupcount = popupcount + 1
  end
  for _, _ in ipairs(mousedecay) do
    mousedecaycount = mousedecaycount + 1
  end
  -- PopUpCount mit Obergrenze vergleichen --
  if popupcount == maxpopups then
    for i in pairs(popups) do
    popups[i] = nil
    end
    combomult = 1
    combo.combofail = 255
    combo.combosuccess = 0
    bluescreen = true
    themesong:stop()
    bluescreensound:play()
    nextbluescreen = love.math.random(1,1)
    resetStats()
    timer.cancel(timers.gentimer)
    timer.after(5,function () 
        lives = 3
        bluescreen = false 
        timers.gentimer = timer.every(gentimerinterval,function () popupClass.generate() end) 
        end)
  end
  for i,popup in pairs(popups) do
    if popup.dragging == true and popup.draggable == true then
      popups[i].x = love.mouse.getX() - popups[i].width/2
      popups[i].y = love.mouse.getY() - popups[i].height/2
    end
    if popup.draggable == true and popup.x >= dragarea.x and popup.x <= dragarea.x+dragarea.width and popup.y+40 <= dragarea.y+dragarea.height then
      popup.rhp = 0
      mouseDecayClass.add(love.mouse.getX(),200,true)
      closedbydrag = true
      timer.after(1,function () closedbydrag=false end)
    end
    if popup.malware == true and popup.malwaremoved == false then
      if popup.y <= 0 then
        flux.to(popup,5,{y=wheight+200})
        timer.after(3.5,function () popup.rhp = 0 end)
      elseif popup.y >= wheight then
        flux.to(popup,5,{y=0-popup.height-200})
        timer.after(3.5,function () popup.rhp = 0 end)
      end
      popup.malwaremoved = true
    end
    if popup.rhp <= 0 and popup.closed == false and not popup.malware then
      popups[i].closed = true
      flux.to(popups[i],0.2,{transparency=0})
      flux.to(popups[i],5,{canvasscale=0})
      highscore = highscore + 1*combomult
      globals.highscore = globals.highscore + 1*combomult
      if goldcombo >= 1 then
        bitcoins = (math.ceil(bitcoins + ((1*stage*goldcombo)*combomult)/5+stage))
        addedcoins = math.ceil(((1*stage*goldcombo)*combomult)/5+stage)
      else
        bitcoins = (math.ceil(bitcoins + ((1*stage)*combomult)/5+stage))
        addedcoins = math.ceil(((1*stage)*combomult)/5+stage)
      end
      
      combo.combosuccess = 255
      combotextdecay = 255
      combomult = combomult + 1 + comboupgrade
      
      totalclosed=totalclosed + 1
      
      totalearned = totalearned + addedcoins
      globals.bitcoins = globals.bitcoins + addedcoins
      coin[love.math.random(1,2)]:play()
      
      local bosschance = love.math.random(1,100)
      -- Stage --
      
      if totalclosed < 9 then
        stage = 1
      elseif  totalclosed >= 9 then
        stage = math.ceil((totalclosed/10))
      end
      
      if stage > laststage then
        flux.to(stagesplash,1.5,{x=0}):ease("expoout"):after(1.5,{x=wwidth}):ease("expoin"):oncomplete(function () stagesplash.x = -wwidth end)
        laststage = stage
        timer.cancel(timers.gentimer)
        gentimerinterval = 60/(20+stage)
        timers.gentimer = timer.every(gentimerinterval,function () popupClass.generate() end)
      end
    elseif popup.rhp <= 0 and popup.closed == false and popup.malware then
      popups[i].closed = true
      if clickedmalware == true then
        local prebit = bitcoins
        bitcoins = math.floor(bitcoins * 0.9)
        addedcoins = bitcoins - prebit
        globals.bitcoins = globals.bitcoins + addedcoins
      end
      flux.to(popups[i],0.2,{transparency=0})
      flux.to(popups[i],5,{canvasscale=0})
    end
    
    if popup.canvasscale == 0 then
      flux.to(popup,0.2,{canvasscale=1}):ease("cubicin")
    end
    if popup.transparency == 1 then
      flux.to(popup,0.5,{transparency=150})
    elseif popup.transparency == 0 then
      table.remove(popups,i)
    end
  end 
  -- Mousedecay --
  if mousedecaycount >= 2 then
     table.remove(mousedecay,1)
  end
  for _,mouse in ipairs(mousedecay) do
    if mouse.transparency > 0 then
      flux.to(mouse,0.75,{transparency=0}):delay(0.25)
      break
    end
    if mouse.transparency == 0 then
      table.remove(mousedecay,1)
      break
    end
  end
  -- Zurücksetzen --
  popupclosed=false
  clickedmalware = false
  speedupcost = math.pow(speedup,3)
  ramupcost = math.pow(ramup,3)
  -- Combo Fail Screen --
  if combo.combofail > 0 then
    flux.to(combo,1,{combofail=0})
  end
  if combo.combosuccess > 0 then
    flux.to(combo,1,{combosuccess=0})
  end
  end
  end
end

function love.draw()
  if gamepaused == false then 
  if introplayed == false and introvideo:isPlaying() then
    love.graphics.setColor(255,255,255,255)
    love.graphics.draw(introvideo,0,0,0,wwidth/introvideo:getWidth(),wheight/introvideo:getHeight())
    love.graphics.setFont(bigfont)
    love.graphics.setColor(255,255,255,introtextopacity.op)
    love.graphics.print("Touch To Skip",wwidth-360,wheight-60)
    --love.graphics.print("Tell: " .. introvideo:tell(),wwidth-360,wheight-120)
  elseif introplayed == true then
  if bluescreen == false then
  -- Text --
  love.graphics.setCanvas(debugtext)
  love.graphics.clear()
  love.graphics.setFont(smallfont)
  love.graphics.setColor(255,255,255)
  love.graphics.print("OS: " .. operatingsystem .. " | Width: " .. wwidth .. " | Height: " .. wheight,0,0)
  love.graphics.print("Bitcoins: " .. bitcoins,0,15)
  love.graphics.print("Stage: " .. stage,0,30)
  love.graphics.print("Speed LVL: " .. speedup .. " || RAM LVL " .. ramup,0,45)
  love.graphics.print("Interval: " .. math.floor(gentimerinterval*100)*0.01,0,60)
  love.graphics.print("Highscore: " .. highscore,0,75)
  love.graphics.print("PopUps: " .. popupcount .. "/" .. maxpopups .. " || RAM: " .. math.floor((((popupcount)/maxpopups)*100)*10)*0.1 .. "%",0,90)
  --love.graphics.print("Quickmenu Opened: " .. quickmenu.opened,0,105)
  love.graphics.setCanvas()
  --love.graphics.draw(debugtext)
  -- Background --
  love.graphics.setCanvas()
  love.graphics.setShader()
  if activebackground == 1 then
  love.graphics.draw(backgrounds.wiese,0,0,0,wwidth/1280,wheight/720)
elseif activebackground == 2 then
  love.graphics.draw(backgrounds.colorful_sands,0,0,0,wwidth/1280,wheight/720)
elseif activebackground == 3 then
  love.graphics.draw(backgrounds.africa,0,0,0,wwidth/1280,wheight/720)
elseif activebackground == 4 then
  love.graphics.draw(backgrounds.alpine,0,0,0,wwidth/1280,wheight/720)
elseif activebackground == 5 then
  love.graphics.draw(backgrounds.snowflakes,0,0,0,wwidth/1280,wheight/720)  
end
  if brightness == 1 then
    love.graphics.setColor(0,0,0,45)
  elseif brightness == 2 then
    love.graphics.setColor(0,0,0,30)
  elseif brightness == 3 then
    love.graphics.setColor(0,0,0,15)
  elseif brightness == 4 then
    love.graphics.setColor(0,0,0,0)
  end
  love.graphics.rectangle("fill",0,0,wwidth,wheight)
  -- Dragarea --
  love.graphics.setColor(0,0,255,dragarea.opacity)
  love.graphics.rectangle("fill",dragarea.x,dragarea.y,dragarea.width,dragarea.height+35)
  
  -- Bitcoin Icon --
  love.graphics.setColor(0,0,0,200)
  love.graphics.rectangle("fill",bitcoinpos.x+24*windowscalex,bitcoinpos.y+8*windowscaley,160*windowscalex,32*windowscaley,16*windowscalex,16*windowscaley)
  -- Coinarea --
  local green = false
  local red = false
  love.graphics.setLineWidth(5)
  if coinarea.opacity > 0 then
    if bitcoins > lastbit then
      green = true
      timer.after(1,function () green = false end)
      flux.to(coinarea,0.6,{opacity=0})
    elseif bitcoins < lastbit then
      red = true
      timer.after(1,function () red = false end)
      flux.to(coinarea,0.6,{opacity=0})
    end
    if green == true then
      love.graphics.setColor(41, 183, 26, coinarea.opacity)
      love.graphics.rectangle ("line", bitcoinpos.x+24*windowscalex,bitcoinpos.y+8*windowscaley,160*windowscalex,32*windowscaley,16*windowscalex,16*windowscaley)
    elseif red == true then
      love.graphics.setColor(210, 30, 30, coinarea.opacity)
      love.graphics.rectangle ("line", bitcoinpos.x+24*windowscalex,bitcoinpos.y+8*windowscaley,160*windowscalex,32*windowscaley,16*windowscalex,16*windowscaley)
    end
  
    timer.after(0.6,function () lastbit = bitcoins end)
  end
  love.graphics.setColor(255,255,255,255)
  love.graphics.draw(textures.bitcoin,bitcoinpos.x,bitcoinpos.y,0,0.2*windowscalex)
  love.graphics.setFont(smallfont)
  if bitcoins > 1000000 then 
    love.graphics.print(math.ceil(10*(bitcoins/1000000))*0.1 .. "m",bitcoinpos.x+56*windowscalex,bitcoinpos.y+14*windowscaley)
  else
    love.graphics.print(bitcoins,bitcoinpos.x+56*windowscalex,bitcoinpos.y+14*windowscaley)
  end
  -- Download Icon --
  love.graphics.setColor(0,0,0,200)
  love.graphics.rectangle("fill",bitcoinpos.x+(24-260)*windowscalex,bitcoinpos.y+8*windowscaley,200*windowscalex,32*windowscaley,16*windowscalex,16*windowscaley)
  love.graphics.setColor(255,255,255,255)
  love.graphics.draw(textures.download,bitcoinpos.x-260*windowscalex,bitcoinpos.y,0,0.2*windowscalex)
  love.graphics.setFont(smallfont)
  love.graphics.print(math.floor(60/gentimerinterval) .. " per minute",bitcoinpos.x+(38-240)*windowscalex,bitcoinpos.y+14*windowscaley)
  -- Combo Icon --
  love.graphics.setColor(0,0,0,200)
  love.graphics.rectangle("fill",bitcoinpos.x+(24+400)*windowscalex,bitcoinpos.y+8*windowscaley,130*windowscalex,32*windowscaley,16*windowscalex,16*windowscaley)
  love.graphics.setColor(255,255,255,255)
  love.graphics.setFont(smallfont)
  love.graphics.print(tostring(combomult),bitcoinpos.x+(88+400)*windowscalex,bitcoinpos.y+14*windowscaley)
  local cgreen = false
  local cred = false
  love.graphics.setLineWidth(5)
  if comboarea.opacity > 0 then
    if combomult > lastcombo then
      cgreen = true
      timer.after(1,function () cgreen = false end)
      flux.to(comboarea,0.6,{opacity=0})
    elseif combomult < lastcombo then
      cred = true
      timer.after(1,function () cred = false end)
      flux.to(comboarea,0.6,{opacity=0})
    end
    if cgreen == true then
      love.graphics.setColor(41, 183, 26, comboarea.opacity)
      love.graphics.rectangle ("line", bitcoinpos.x+(24+400+15)*windowscalex,bitcoinpos.y+8*windowscaley,115*windowscalex,32*windowscaley,16*windowscalex,16*windowscaley)
    elseif cred == true then
      love.graphics.setColor(210, 30, 30, comboarea.opacity)
      love.graphics.rectangle ("line", bitcoinpos.x+(24+400+15)*windowscalex,bitcoinpos.y+8*windowscaley,115*windowscalex,32*windowscaley,16*windowscalex,16*windowscaley)
    end
  
    timer.after(0.6,function () lastcombo = combomult end)
  end
  love.graphics.setColor(255,255,255,255)
  love.graphics.draw(textures.combo_icon,bitcoinpos.x+(24+400)*windowscalex,bitcoinpos.y,0,0.2*windowscalex)
  -- Highscore --
  local hgreen = false
  local hred = false
  love.graphics.setColor(0,0,0,200)
  love.graphics.rectangle("fill",bitcoinpos.x+(24+210)*windowscalex,bitcoinpos.y+8*windowscaley,160*windowscalex,32*windowscaley,16*windowscalex,16*windowscaley)
  love.graphics.setLineWidth(5)
  if coinarea.opacity > 0 then
    if highscore > lasthighscore then
      hgreen = true
      timer.after(1,function () green = false end)
      flux.to(higharea,0.6,{opacity=0})
    elseif highscore < lasthighscore then
      hred = true
      timer.after(1,function () red = false end)
      flux.to(higharea,0.6,{opacity=0})
    end
    if hgreen == true then
      love.graphics.setColor(41, 183, 26, higharea.opacity)
      love.graphics.rectangle ("line", bitcoinpos.x+(24+210)*windowscalex,bitcoinpos.y+8*windowscaley,160*windowscalex,32*windowscaley,16*windowscalex,16*windowscaley)
    elseif hred == true then
      love.graphics.setColor(210, 30, 30, higharea.opacity)
      love.graphics.rectangle ("line", bitcoinpos.x+(24+210)*windowscalex,bitcoinpos.y+8*windowscaley,160*windowscalex,32*windowscaley,16*windowscalex,16*windowscaley)
    end
  
    timer.after(0.6,function () lasthighscore = highscore end)
  end
  
  love.graphics.setColor(255,255,255,255)
  love.graphics.draw(textures.highscore,bitcoinpos.x+210*windowscalex,bitcoinpos.y,0,0.2*windowscalex)
  if highscore > 1000000 then 
    love.graphics.print(math.ceil(10*(highscore/1000000))*0.1 .. "m",bitcoinpos.x+(56+210)*windowscalex,bitcoinpos.y+14*windowscaley)
  else
    love.graphics.print(highscore,bitcoinpos.x+(56+210)*windowscalex,bitcoinpos.y+14*windowscaley)
  end
  -- Combo Text --
  love.graphics.setCanvas(combotext)
  love.graphics.clear()
  love.graphics.setFont(bigfont)
  love.graphics.setColor(255,255,255)
  love.graphics.print(combomult .. "x")
  love.graphics.setFont(smallfont)
  love.graphics.setColor(255,255,255)
  love.graphics.setCanvas()
  -- Bomb --
  love.graphics.setColor(0,0,0,200)
  love.graphics.rectangle("fill",wwidth*1/15+24*windowscalex,bitcoinpos.y+8*windowscaley,124*windowscalex,32*windowscaley,16*windowscalex,16*windowscaley)
  love.graphics.setColor(255,255,255,255)
  love.graphics.draw(textures.bomb,wwidth*1/15-20*windowscalex,bitcoinpos.y-36*windowscaley,0,0.4*windowscalex)
  love.graphics.print("x" .. tostring(bomb),wwidth*1/15+78*windowscalex,bitcoinpos.y+14*windowscalex)
  -- PopUps --
  popupClass.draw()
  -- Mousedecay --
  mouseDecayClass.draw()
  love.graphics.setColor(255,255,255)
  love.graphics.setLineWidth(1)
  -- UpgradeKnopf --
  if quickmenu.opened == false and quickmenuitems[1].opened == false and quickmenuitems[2].opened == false and quickmenuitems[3].opened == false and quickmenuitems[4].opened == false then
    love.graphics.setColor(0,0,0,200)
    love.graphics.rectangle("fill",menuButton.x,menuButton.y,menuButton.width,menuButton.height)
    love.graphics.setColor(255,255,255)
    love.graphics.print("UPGRADE",menuButton.x+6,menuButton.y)
  end
  -- Life --
  for i=lives,1,-1 do
    love.graphics.draw(textures.kopf,wwidth-150-i*55*windowscalex,wheight*1/20-15*windowscaley,0,0.4*windowscalex)
  end
  -- UpgradeMenu --
  if quickmenu.x < wwidth then
    upgradeClass.draw()
  end
  -- Achievements --
  achievementClass.draw()
  -- RAM-Anzeige --
  love.graphics.setFont(smallfont)
  love.graphics.setColor(255,255,255,200)
  love.graphics.rectangle("line",50*windowscalex,wheight*1/20,150*windowscalex,30*windowscaley)
  love.graphics.setColor(0,0,0,200)
  love.graphics.rectangle("fill",51*windowscalex,wheight*1/20+1,148*windowscalex,28*windowscaley)
  if popupcount/maxpopups >= 0.8 then
    love.graphics.setColor(255,255,255)
    love.graphics.draw(textures.warning,96,wheight*2/20,0,0.125*windowscalex)
    love.graphics.setColor(255,0,0,200)
  else
    love.graphics.setColor(0,200,100,200)
  end
  love.graphics.rectangle("fill",51*windowscalex,wheight*1/20+1,(148*(popupcount/maxpopups))*windowscalex,28*windowscaley)
  love.graphics.setColor(255,0,0)
  love.graphics.setColor(255,255,255)
  if maxpopups < 100 then
    love.graphics.print(popupcount .. "/" .. maxpopups,104*windowscalex,wheight*1/20+4)
  else
    love.graphics.print(popupcount .. "/" .. maxpopups,94*windowscalex,wheight*1/20+4)
  end
  -- Gold Timer --
  if timers.goldtimertime > 0 then
    love.graphics.setColor(0,0,0,200)
    love.graphics.rectangle("fill",250*windowscalex,wheight*1/20,150*windowscalex,30)
    love.graphics.setColor(218,165,32)
    love.graphics.rectangle("fill",250*windowscalex,wheight*1/20,(150*(timers.goldtimertime/770))*windowscalex,30)
    love.graphics.setColor(255,255,255)
    love.graphics.rectangle("line",250*windowscalex,wheight*1/20,150*windowscalex,30)
    love.graphics.draw(textures.bitcoin,230*windowscalex,wheight*1/20-10,0,0.2*windowscalex)
  end
  love.graphics.setColor(0,0,0)
  --love.graphics.print(tostring(globals.highscore),0,0)
  --love.graphics.print(tostring(dragging.y),0,20)
  -- Tutorial --
  if tutorialcompleted == false then
    tutorialClass.draw()
  end
  
  -- Mauszeiger --
  if operatingsystem == 'Windows' then
    love.graphics.setColor(255,255,255)
    love.graphics.circle("fill",love.mouse.getX(),love.mouse.getY(),8,8)
    love.graphics.setColor(100,100,100)
    love.graphics.circle("line",love.mouse.getX(),love.mouse.getY(),8,8)
  end
  
  -- Stage Splash --
  love.graphics.setColor(0,0,0,100)
  love.graphics.rectangle("fill",stagesplash.x,(wheight/2)-100,wwidth,200)
  love.graphics.setFont(hugefont)
  love.graphics.setColor(255,255,255)
  love.graphics.print("Stage " .. tostring(stage),stagesplash.x+wwidth/2-280,wheight/2-90)
  
  
  
  
  elseif bluescreen == true then
    love.graphics.setColor(255,255,255)
    love.graphics.draw(bluescreens[nextbluescreen],0,0,0,wwidth/1280,wheight/720)
  end
  -- FPS CAP Part 3 --
  local cur_time = love.timer.getTime()
   if next_time <= cur_time then
      next_time = cur_time
      return
   end
   love.timer.sleep(next_time - cur_time)
   -- FPS CAP ENDE Part 3 --
  end
  end
end