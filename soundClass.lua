local soundClass = {}
themesong = love.audio.newSource("sfx/ed_theme_demo.ogg","stream")
applause = {love.audio.newSource("sfx/applause/applause (1).ogg"),love.audio.newSource("sfx/applause/applause (2).ogg"),love.audio.newSource("sfx/applause/applause (3).ogg"),love.audio.newSource("sfx/applause/applause (4).ogg")}
coin = {love.audio.newSource("sfx/coin (1).ogg","static"),love.audio.newSource("sfx/coin (2).ogg","static")}
bluescreensound = love.audio.newSource("sfx/bluescreen.ogg","static")
miss = {love.audio.newSource("sfx/miss/miss 2.ogg"),love.audio.newSource("sfx/miss/miss 3.ogg")}
buysound = love.audio.newSource("sfx/cash register.ogg")
bombsound = love.audio.newSource("sfx/bomb.ogg")
menu = {select=love.audio.newSource("sfx/menu select.ogg"),back=love.audio.newSource("sfx/menu back.ogg")}
gold = {spawn=love.audio.newSource("sfx/goldspawn.ogg"),click=love.audio.newSource("sfx/goldclick.ogg")}
popupclick = love.audio.newSource("sfx/click.wav")
themesong:setVolume(0.2)
function soundClass.playTheme()
  if themesong:play() == false then
    themesong:play()
  end
end
return soundClass